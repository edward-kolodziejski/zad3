---
author: Edward Kolodziejski
title: Khruangbin
subtitle: Mój ulubiony zespół
date: 
theme: Malmoe
output: beamer_presentation
header-includes: 
    \usepackage{xcolor}
    \usepackage{listings}
fontsize: 8pt
---


## Khruangbin - o zespole 

 *Khruangbin* to amerykański zespół muzyczny założony w 2010 roku w Burton w stanie Teksas z inicjatywy Marka Speera i Laury Lee. Zespół gra w składzie: Mark Speer – gitara solowa, Laura Lee – gitara basowa i Donald Johnson, Jr. – perkusja. Prezentuje melodyjną, głównien instrumentalną muzykę, opartą na soulu i rhythm and bluesie, inspirowaną tajskim rockiem i funkiem z lat 60. i 70., a także wieloma innymi gatunkami i technikami, takimi jak: dub, muzyka hiszpańska, irlandzka, jamajska (reggae) Północnej Afryki i Bliskiego Wschodu (Iran, Afganistan).

Nazwa pochodzi z języka tajskiego i oznacza **latającą maszynę**, **aeroplan** lub **samolot**.


## Khruangbin - dyskografia

Albumy studyjne:

1. The Universe Smiles upon You
2. Con Todo El Mundo
3. Mordechai

EPki:

* The Infamous Bill
* History Of Flight
* Spotify Singles
* Texas Sun (with Leon Bridges)

Remiksy:

* Hasta El Cielo 
    * (2019, Dead Oceans; Night Time Stories)
* Late Night Tales: Khruangbin
    * (2020, Night Time Stories)


## Khruangbin - single

| Tytuł                                | Rok  | Album/EP                     |
|:------------------------------------ |:----:|:----------------------------:|
| "A Calf Born in Winter"              | 2014 | Singiel bez albumu           |
| "White Gloves"                       | 2015 | The Universe Smiles Upon You |
| Maria También"                       | 2017 |  Con Todo El Mundo           |
| "Friday Morning"                     | 2018 | Con Todo El Mundo            |
| "Christmas Time Is Here"             | 2018 | Singiel bez albumu           |
| "Texas Sun" (with Leon Bridges)      | 2019 |    Texas Sun                 |
| "Time (You and I)"                   | 2020 |    Mordechai                 |
| "So We Won't Forget"                 | 2020 | Mordechai                    |
| "Pelota"                             | 2020 |   Mordechai                  |
| "Summer Madness" (with Leon Bridges) | 2020 | Late Night Tales: Khruangbin |


## Khruangbin - skład

\begin{center}Mark Speer, Laura Lee, Donald "DJ" Johnson\end{center}

![alt text](https://media.npr.org/assets/img/2018/01/24/khruangbin_bymarykang2_wide-ad32a42357b817e913d3cd169e46c3b2609a6445.jpg?s=1400) *www.media.npr.org*


## Khruangbin - strona internetowa

\begin{block}{Khruangbin}
Oficjalna strona internetowa
\end{block}
[https://www.khruangbin.com/](https://www.khruangbin.com/)


## Bibliografia

Wszystkie treści pochodzącą z artykułu Wikipedia 
[https://en.wikipedia.org/wiki/Khruangbin](https://en.wikipedia.org/wiki/Khruangbin)